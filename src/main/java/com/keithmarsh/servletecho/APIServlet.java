package com.keithmarsh.servletecho;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class APIServlet extends HttpServlet {
	private static final long serialVersionUID = -1836233223320449451L;
	private static final Logger mLog = Logger.getLogger(APIServlet.class.getName());
    

    public void init(ServletConfig config) throws ServletException {
        mLog.entering("APIServlet", "init");
        mLog.info(debugServletConfig(config));
    }
        
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String debug = debugRequest("doGet", request); 
        debug += "ServletInfo : " + getServletInfo() + "\n";
        mLog.info(debug);
        response.getWriter().println(debug);
        response.getWriter().close();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String debug = debugRequest("doPost", request); 
        mLog.info(debug);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        debugRequest("doPut", request);
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        debugRequest("doDelete", request);
    }
    
    @SuppressWarnings("unchecked")
    public String debugRequest(String moduleName, HttpServletRequest request) {
        StringBuilder body = new StringBuilder("---------- " + moduleName + " -----------\n");
        Enumeration<String> overNames = request.getParameterNames();
        body.append(request.getRequestURL());
        if (request.getQueryString() != null) {
            body.append("?" + request.getQueryString());
        }
        body.append("\nContextPath : " + request.getContextPath());
        body.append("\nLocalAddr : " + request.getLocalAddr());
        body.append("\nLocalName : " + request.getLocalName());
        body.append("\nPathInfo : " + request.getPathInfo());
        body.append("\nPathTranslated : " + request.getPathTranslated());
        body.append("\nRemoteAddr : " + request.getRemoteAddr());
        body.append("\nRemotePort : " + request.getRemotePort());
        body.append("\nRemoteUser : " + request.getRemoteUser());
        body.append("\nRequestURI : " + request.getRequestURI());
        body.append("\nRequestURL : " + request.getRequestURL());
        body.append("\nServerName : " + request.getServerName());
        body.append("\nServletPath : " + request.getServletPath() + "\n");
        if (overNames.hasMoreElements()) {
            body.append("Params\n");
            while (overNames.hasMoreElements()) {
                String name = overNames.nextElement();
                body.append("  ");
                body.append(name);
                body.append(" : ");
                body.append(request.getParameter(name));
                body.append("\n");
            }
        }
        body.append("Headers\n");
        overNames = (Enumeration<String>) request.getHeaderNames();
        while (overNames.hasMoreElements()) {
            String headerName = overNames.nextElement();
            String headerValue = request.getHeader(headerName); 
            body.append("  ");
            body.append(headerName);
            body.append(" : ");
            body.append(headerValue);
            body.append("\n");
        }
        overNames = request.getSession().getAttributeNames();
        if (overNames.hasMoreElements()) {
            body.append("Session\n");
            while (overNames.hasMoreElements()) {
                String headerName = overNames.nextElement();
                body.append("  ");
                body.append(headerName);
                body.append(" : ");
                body.append(request.getSession().getAttribute(headerName));
                body.append("\n");
            }
        }
        
        StringBuilder data = new StringBuilder();
        try {
            String line;
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                data.append("  " + line + "\n");
        } catch (Exception e) {
            mLog.log(Level.SEVERE, "Can't read body", e);
        }
        if (data.length() > 0) {
            body.append("Body\n");
            body.append(data);
        }
        return body.toString();
    }

    public String debugServletConfig(ServletConfig config) {
        StringBuilder debug = new StringBuilder("ServletConfig\n  ServletName : " + config.getServletName() + "\n");
        debug.append("  InitParameters:\n");
        @SuppressWarnings("unchecked")
		Enumeration<String> overNames = config.getInitParameterNames();
        while (overNames.hasMoreElements()) {
        	String name = overNames.nextElement();
        	debug.append("    " + name + " : " + config.getInitParameter(name) + "\n");
        }
    	debug.append(debugServletContext(config.getServletContext()));
    	return debug.toString();
    }
    
    @SuppressWarnings("unchecked")
	public String debugServletContext(ServletContext context) {
    	StringBuilder debug = new StringBuilder("  ServletContext\n");
    	debug.append("    serverInfo : " + context.getServerInfo() + "\n");
    	debug.append("    contextPath : " + context.getContextPath() + "\n");
    	debug.append("    majorVersion : " + context.getMajorVersion() + "\n");
    	debug.append("    minorVersion : " + context.getMinorVersion() + "\n");
    	debug.append("    realPath : " + context.getRealPath("/index.html") + "\n");
    	debug.append("    resourcePaths : " + context.getResourcePaths("/") + "\n");
    	debug.append("    attributeNames:\n");
		Enumeration<String> overNames = context.getAttributeNames();
        while (overNames.hasMoreElements()) {
        	String name = overNames.nextElement();
        	debug.append("      " + name + " : " + context.getAttribute(name) + "\n");
        }
    	debug.append("    initParameterNames:\n");
        overNames = context.getInitParameterNames();
        while (overNames.hasMoreElements()) {
        	String name = overNames.nextElement();
        	debug.append("      " + name + " : " + context.getAttribute(name) + "\n");
        }
    	return debug.toString();
    }

}
